<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgentbiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agentbies', function (Blueprint $table) {
            $table->increments('id');
              $table->string('name');
              $table->string('email');
              $table->string('mobile');
              $table->string('address');
            $table->boolean('edit')->nullable()->default(0);
            $table->unsignedInteger('agent')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agentbies');
    }
}
