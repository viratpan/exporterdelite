<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgentItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_items', function (Blueprint $table) {
            $table->increments('id');
             $table->string('name');
              $table->string('type')->nullable()->comment('type');
                $table->decimal('rate', 12, 2)->nullable()->comment('rate price');
                $table->decimal('amount', 12, 2)->nullable()->comment('total price');             
              $table->string('qty');
               $table->unsignedInteger('agentby')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agent_items');
    }
}
