@extends('layouts.app')
@section('style')
<style type="text/css">
  form label {
    font-size: 13px !important;
    color:#777;
  }
  form .form-group {
      margin-bottom: 0.2rem !important;
      padding: 6px 0!important;
  }
  label {
      display: inline-block;
      margin-bottom: .2rem;
  }
  input.form-control {
      height: 32px;
      font-size: 12px;padding:5px;
  }
</style>
@endsection
@section('script')
<script type="text/javascript">
  var count=0;
   $(document).ready(function () {
    @foreach($dataItem as $value)
        additem('{{ $value->id}}','{{ $value->name}}','{{ $value->type}}','{{ $value->rate}}','{{ $value->qty}}','{{ $value->amount}}');
    @endforeach
    
  
    $('#thisAgent').change(function(e) {
     var id=$(this).val();
     if(id){
        $.get('{{ route('ajax.agent-data') }}', 
          { 'id': id }, 
          function( res ) {
            if(res){ //alert(res.email)
                $("#email").val(res.email); $("#mobile").val(res.mobile);$("#address").val(res.address);
                
            }else{
               $("#email").val(''); $("#mobile").val('');$("#address").val('');
            }
            
            
          }
        );
     }
    /////     
    });
  });
  function additem(id='',name='',type='',rate=0,qty=1,ammount=0) {      

    var html=`<div class="form-row  itemDiv" id="itemDiv`+count+`">
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Item Name <span class="required">*</span></label>
                  <input type="text" value="`+name+`" name="item[`+count+`][name]" class="form-control" required placeholder="Item Name">
                  <input type="hidden" value="`+id+`" name="item[`+count+`][id]" >
              </div>
              <div class="form-group col-md-3">
                  <label for="inputEmail4">Type<span class="required">*</span></label>
                  <input type="text" value="`+type+`"  name="item[`+count+`][type]" class="form-control" required placeholder="Item Type">
              </div>
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Rate<span class="required">*</span></label>
                  <input type="number" step=".01" id="rate`+count+`" value="`+rate+`" onblur="calculate(`+count+`);" name="item[`+count+`][rate]" class="form-control" required placeholder="Rate">
              </div>
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Qty<span class="required">*</span></label>
                  <input type="number"  id="qty`+count+`" onblur="calculate(`+count+`);" name="item[`+count+`][qty]" class="form-control" value="`+qty+`" required placeholder="Qty">
              </div>
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Amount<span class="required">*</span></label>
                  <input type="number" step=".01" value="`+ammount+`" id="amount`+count+`" onblur="calculate(`+count+`);" name="item[`+count+`][amount]" class="form-control" required placeholder="Amount">
              </div>
              <div class="form-group col-md-1 float-right" style="padding-top: 2rem!important;">
                <label for="inputEmail4"></label>
                  <button type="button" onclick="remove(this);" class="btn btn-danger ">-</button>                  
              </div>
            </div>`;
        $('#items').before(html);
        calculate(count);
       
        count++;

  }
  function remove(this_obj) {
    $(this_obj).parents('.itemDiv').remove();
  }
  function calculate(divId) {
    var price=$('#itemDiv'+divId).find('#rate'+divId).val();
    var qty=$('#itemDiv'+divId).find('#qty'+divId).val();
    $('#itemDiv'+divId).find('#amount'+divId).val(price*qty);
    
  }
  function verify() { alert('lunch model');
    $('#inlineForm').model('show');
  }
</script>
@endsection

@section('content')


<div class="card ">
  <div class="card-header">
    <div>
        <form method="{{ isset($data) ? 'PUT': "POST"}}" action="{{ route('agentItem') }}{{ isset($data) ? '/update' : "/store"}}{{ isset($data) ? '/'.$data->id : ""}}">
          @csrf
          <div class="form-row" style="border-bottom: 2px solid black;">
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Name<span class="required">*</span></label>
                  {{-- <input type="text" value="{{ old('name') }}{{ isset($data) ? $data->name : ""}}" name="name" class="form-control" required placeholder="Name"> --}}
                  <select id="thisAgent" name="name" class="form-control" required>
                    <option value="">Please Select</option>
                    @foreach($agent as $val)
                      <option value="{{ $val->id }}">{{ $val->name }}</option>
                    @endforeach
                  </select>
              </div>
              <div class="form-group col-md-3">
                  <label for="inputEmail4">Email<span class="required">*</span></label>
                  <input type="text" id="email" value="{{ old('email') }}{{ isset($data) ? $data->email : ""}}" name="email" class="form-control" required placeholder="Email">
              </div>
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Mobile<span class="required">*</span></label>
                  <input type="text" id="mobile" value="{{ old('mobile') }}{{ isset($data) ? $data->mobile : ""}}" name="mobile" class="form-control" required placeholder="Mobile">
              </div>
              <div class="form-group col-md-4">
                  <label for="inputEmail4">Address<span class="required">*</span></label>
                  <input type="text" id="address" value="{{ old('address') }}{{ isset($data) ? $data->address : ""}}" name="address" class="form-control" required placeholder="Address">
              </div>
             
               <div class="form-group col-md-1 float-right" style="padding-top: 2rem!important;">
                <label for="inputEmail4"></label>
                  <button type="button" onclick="additem();" class="btn btn-primary ">+</button>                  
              </div>
            </div>
            
            <div class="form-row" id="items">          
               
              <div class="form-group col-md-12 float-right" style="padding-top: 2rem!important;">
                <label for="inputEmail4"></label>
                  <button type="submit" class="btn btn-primary ">Submit</button>
                  @if(isset($data))
                    <a href="{{ route('agentItem') }}" class="btn btn-info ">Cancel</a>
                  @endif
              </div>
          </div>                 
          
      </form>
    </div>

  </div>
  <div class="card-body">
    <?php //echo"<pre>",print_r($data),"</pre>";?>
    <div class="table-responsive">
        <table id="data_table" class="table data_table">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th> 
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>GST</th>
                    <th>PAN</th>             
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($agentby as $uData)
                <tr>
                    <td>{{ $uData->id  }}</td>
                    <td>{{ $uData->name  }}</td>
                    <td>{{ $uData->email  }}</td>
                    <td>{{ $uData->mobile  }}</td>
                    <td>{{ $uData->address  }}</td>
                    
                    <td>
                        <a class="btn btn-primary confirm" data-id="{{$uData->id}}" data-href="{{ route('agentItem.edit',$uData->id) }}">Edit</a>
                           
                      
                    </td>
                </tr>
            @endforeach    
                
            </tbody>
           {{--  <tfoot>
                <tr>
                   <th>Id</th>
                    <th>Name</th>
                    
                    <th>Action</th>
                </tr>
            </tfoot> --}}
        </table>

        
    </div>
  </div>
  <div class="card-footer text-muted">
{{--     {!! $uData->links() !!} --}}
{{--  {{ $uData->links() }} --}}
  </div>
</div>
	


  <!-- Modal -->
                  <div class="modal fade text-left" id="inlineForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33"
                   aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <label class="modal-title text-text-bold-600" id="myModalLabel33">Confirmation</label>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <form action="#">
                          <div class="modal-body">
                           
                            <label>Password: </label>
                            <div class="form-group">
                              <input type="password" placeholder="Password" class="form-control">
                            </div>
                          </div>
                          <div class="modal-footer">
                            <input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="close">
                            <input type="submit" class="btn btn-outline-primary btn-lg" value="Login">
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
   @endsection
