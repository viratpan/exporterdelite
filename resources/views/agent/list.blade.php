@extends('layouts.app')
@section('style')
<style type="text/css">
  form label {
    font-size: 13px !important;
    color:#777;
  }
  form .form-group {
      margin-bottom: 0.2rem !important;
      padding: 6px 0!important;
  }
  label {
      display: inline-block;
      margin-bottom: .2rem;
  }
  input.form-control {
      height: 32px;
      font-size: 12px;padding:5px;
  }
</style>
@endsection


@section('content')


<div class="card ">
  <div class="card-header">
    <div>
        <form method="{{ isset($data) ? 'PUT': "POST"}}" action="{{ route('agent') }}{{ isset($data) ? '/update' : "/store"}}{{ isset($data) ? '/'.$data->id : ""}}">
          @csrf
          <div class="form-row">
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Name <span class="required">*</span></label>
                  <input type="text" value="{{ old('name') }}{{ isset($data) ? $data->name : ""}}" name="name" class="form-control" required placeholder="Name">
              </div>
              <div class="form-group col-md-3">
                  <label for="inputEmail4">Email<span class="required">*</span></label>
                  <input type="text" value="{{ old('email') }}{{ isset($data) ? $data->email : ""}}" name="email" class="form-control" required placeholder="Email">
              </div>
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Mobile<span class="required">*</span></label>
                  <input type="text" value="{{ old('mobile') }}{{ isset($data) ? $data->mobile : ""}}" name="mobile" class="form-control" required placeholder="Mobile">
              </div>
              <div class="form-group col-md-2">
                  <label for="inputEmail4">GST<span class="required">*</span></label>
                  <input type="text" value="{{ old('gst') }}{{ isset($data) ? $data->gst : ""}}" name="gst" class="form-control" required placeholder="GST">
              </div>
              <div class="form-group col-md-2">
                  <label for="inputEmail4">PAN<span class="required">*</span></label>
                  <input type="text" value="{{ old('pan') }}{{ isset($data) ? $data->pan : ""}}" name="pan" class="form-control" required placeholder="PAN">
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-8">
                  <label for="inputEmail4">Address<span class="required">*</span></label>
                  <input type="text" value="{{ old('address') }}{{ isset($data) ? $data->address : ""}}" name="address" class="form-control" required placeholder="Address">
              </div>
               
              <div class="form-group col-md-2 float-right" style="padding-top: 2rem!important;">
                <label for="inputEmail4"></label>
                  <button type="submit" class="btn btn-primary ">Submit</button>
                  @if(isset($data))
                    <a href="{{ route('agent') }}" class="btn btn-info ">Cancel</a>
                  @endif
              </div>
          </div>                 
          
      </form>
    </div>

  </div>
  <div class="card-body">
    <?php //echo"<pre>",print_r($data),"</pre>";?>
    <div class="table-responsive">
        <table id="data_table" class="table data_table">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th> 
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>GST</th>
                    <th>PAN</th>             
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($agent as $uData)
                <tr>
                    <td>{{ $uData->id  }}</td>
                    <td>{{ $uData->name  }}</td>
                    <td>{{ $uData->email  }}</td>
                    <td>{{ $uData->mobile  }}</td>
                    <td>{{ $uData->gst  }}</td>
                    <td>{{ $uData->pan  }}</td>
                    <td>
                        <a class="btn btn-primary" href="{{ route('agent.edit',$uData->id) }}">Edit</a>
                           
                      
                    </td>
                </tr>
            @endforeach    
                
            </tbody>
           {{--  <tfoot>
                <tr>
                   <th>Id</th>
                    <th>Name</th>
                    
                    <th>Action</th>
                </tr>
            </tfoot> --}}
        </table>

        
    </div>
  </div>
  <div class="card-footer text-muted">
{{--     {!! $uData->links() !!} --}}
{{--  {{ $uData->links() }} --}}
  </div>
</div>
	
   @endsection
