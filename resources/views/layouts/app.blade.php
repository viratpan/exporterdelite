<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta charset="utf-8">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link rel="apple-touch-icon" href="{{ asset(config('app.favicon')) }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset(config('app.favicon')) }}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet">
     <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css" rel="stylesheet">
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    {{-- main layout --}}
        
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.css')}}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/components.min.css') }}">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/colors/palette-gradient.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/charts/jquery-jvectormap-2.0.3.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/charts/morris.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/fonts/simple-line-icons/style.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/colors/palette-gradient.min.css') }}">
    <!-- END: Page CSS-->
    <link rel="stylesheet" href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">
    <!-- END: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/toastr.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/style.css') }}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
     @yield('style')
</head>
@php 
     $currentURl= Route::getFacadeRoot()->current()->uri();
     
     if(in_array($currentURl, config('app.singlepage'))){
        echo '<body class="horizontal-layout horizontal-menu 1-column  bg-full-screen-image blank-page" data-open="hover" data-menu="horizontal-menu" data-col="1-column">';
     }else{
        echo '<body class="horizontal-layout horizontal-menu 2-columns  " data-open="hover" data-menu="horizontal-menu" data-col="2-columns">';
@endphp
        @include('header.topnav')
        @include('header.mainmenu')

@php  }  @endphp

    
    <!-- BEGIN: Content-->
    <div class="app-content content">
      <div class="content-overlay"></div>
      <div class="content-wrapper">
        <div class="content-header row"></div>
        <div class="onSubmitForm" id="gif" style=" visibility: hidden;"><i class="la la-spinner spinner white" align="center" style="font-size:68px;margin-left: 13%;"></i></div>
         <style>
             .onSubmitForm{
                 position: fixed;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
                display: -ms-flexbox;
                display: flex;
                -ms-flex-pack: center;
                justify-content: center;
                -ms-flex-align: center;
                align-items: center;
                z-index: 1;
                background: rgba(0,0,0,.4);
                }
                .onSubmitFormIcon {                    
                    position: fixed;
                    left: 50%;
                    top: 45%;
                    //z-index: 999;
                }
                .add-now {
                    position: fixed;
                    bottom: 5%;
                    right: 2%;
                    z-index: 1051;
                }
                .addList-now {
                    position: fixed;
                    bottom: 10%;
                    right: 2%;
                    max-height: 70vh;
                    
                    z-index: 1051;
                    padding: 11px;
                    list-style: none;
                }
                
         </style>
        <div class="content-body"><!-- Revenue, Hit Rate & Deals -->
            @yield('content')
        </div>
      </div>
    </div>

     <!-- Buynow Button-->
    {{-- <div class="buy-now">
        <a href="https://1.envato.market/modern_admin" target="_blank" class="btn btn-info btn-glow round px-2">Buy Now</a>
    </div> --}}
    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

    <!-- BEGIN: Footer-->
    <footer class="footer footer-static footer-light navbar-shadow">
      <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
        <span class="float-md-left d-block d-md-inline-block">Copyright &copy; 2019 
            <a class="text-bold-800 grey darken-2" >PIXINVENT</a>
        </span>
        <span class="float-md-right d-none d-lg-block">Virat Choudhary<i class="ft-heart pink"></i>
            <span id="scroll-top"></span>
        </span>
      </p>
    </footer>
    <!-- END: Footer-->


    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('app-assets/vendors/js/vendors.min.js') }}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('app-assets/vendors/js/ui/jquery.sticky.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/charts/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/charts/chart.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/charts/raphael-min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/charts/morris.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/charts/jvector/jquery-jvectormap-2.0.3.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/charts/jvector/jquery-jvectormap-world-mill.js') }}"></script>
    <script src="{{ asset('app-assets/data/jvector/visitor-data.js') }}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('app-assets/js/core/app-menu.min.js') }}"></script>
    <script src="{{ asset('app-assets/js/core/app.min.js') }}"></script>
    <script src="{{ asset('app-assets/js/scripts/customizer.min.js') }}"></script>
    <script src="{{ asset('app-assets/js/scripts/footer.min.js') }}"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{ asset('app-assets/js/scripts/ui/breadcrumbs-with-stats.min.js') }}"></script>   
    <script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js')}}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.min.js')}}"></script>
    <!-- END: Page JS-->
    <script type="text/javascript">
    $(document).ready(function () {       
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                toastr.error("{{ $error }}", { closeButton: !0 });
            @endforeach
        @endif
        @if (session('success'))
            toastr.success("{{ session('success') }}", { closeButton: !0 });
        @endif
        @if (session('error'))
            toastr.error("{{ session('error') }}", { closeButton: !0 });
        @endif
        @if (session('warning'))
            toastr.warning("{{ session('warning') }}", { closeButton: !0 });
        @endif

        
         $('#data_table').DataTable({
            dom: "<'row'<'col-sm-12 col-md-4 w-100 datasearch'f><'col-sm-12 col-md-4 data-show-hide'C><'col-sm-12 col-md-3 data-buttons'B>>" +
                "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
                    "lengthMenu": [[-1,50, 100, 150], ["All",50, 100, 150]],
                    "scrollY": 200,
                   // "scrollX": true,
                    buttons: [
                    'pageLength', 'excel', 'pdf','print'
                    ],
                    language: { search: '', searchPlaceholder: "Search..." }
        });
        
    });
</script>
     @yield('script')
</body>
</html>
