 <!-- BEGIN: Main Menu-->

    <div class="header-navbar navbar-expand-sm navbar navbar-horizontal navbar-fixed navbar-dark navbar-without-dd-arrow navbar-shadow" role="navigation" data-menu="menu-wrapper">
      <div class="navbar-container main-menu-content" data-menu="menu-container">
        <ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation">
          <li class=" nav-item" ><a class="nav-link" href="{{ route('home')}}" >
            <i class="la la-home"></i>
            <span data-i18n="Dashboard">Dashboard</span></a>            
          </li>

          <li class=" nav-item" ><a class="nav-link" href="{{ route('agent')}}" >
            <i class="la la-home"></i>
            <span data-i18n="Dashboard">Agent</span></a>            
          </li>
          <li class=" nav-item" ><a class="nav-link" href="{{ route('agentItem')}}" >
            <i class="la la-home"></i>
            <span data-i18n="Dashboard">AgentItem</span></a>            
          </li>

         {{--  <li class="dropdown nav-item" data-menu="dropdown"><a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown"><i class="la la-television"></i><span data-i18n="Templates">Templates</span></a>
            <ul class="dropdown-menu">
              <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#" data-toggle="dropdown"><i class="la la-arrows-v"></i><span data-i18n="Vertical">Vertical</span></a>
                <ul class="dropdown-menu">
                  <li data-menu=""><a class="dropdown-item" href="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/html/ltr/vertical-menu-template" data-toggle=""><span data-i18n="Classic Menu">Classic Menu</span></a>
                  </li>
                  <li data-menu=""><a class="dropdown-item" href="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/html/ltr/vertical-modern-menu-template" data-toggle=""><span data-i18n="Modern Menu">Modern Menu</span></a>
                  </li>
                  <li data-menu=""><a class="dropdown-item" href="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/html/ltr/vertical-collapsed-menu-template" data-toggle=""><span data-i18n="Collapsed Menu">Collapsed Menu</span></a>
                  </li>
                  <li data-menu=""><a class="dropdown-item" href="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/html/ltr/vertical-compact-menu-template" data-toggle=""><span data-i18n="Compact Menu">Compact Menu</span></a>
                  </li>
                  <li data-menu=""><a class="dropdown-item" href="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/html/ltr/vertical-content-menu-template" data-toggle=""><span data-i18n="Content Menu">Content Menu</span></a>
                  </li>
                  <li data-menu=""><a class="dropdown-item" href="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/html/ltr/vertical-overlay-menu-template" data-toggle=""><span data-i18n="Overlay Menu">Overlay Menu</span></a>
                  </li>
                </ul>
              </li>
              <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#" data-toggle="dropdown"><i class="la la-arrows-h"></i><span data-i18n="Horizontal">Horizontal</span></a>
                <ul class="dropdown-menu">
                  <li data-menu=""><a class="dropdown-item" href="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/html/ltr/horizontal-menu-template" data-toggle=""><span data-i18n="Classic">Classic</span></a>
                  </li>
                  <li data-menu=""><a class="dropdown-item" href="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/html/ltr/horizontal-menu-template-nav" data-toggle=""><span data-i18n="Full Width">Full Width</span></a>
                  </li>
                </ul>
              </li>
            </ul>
          </li> --}}
          
          
         


        </ul>
      </div>
    </div>

    <!-- END: Main Menu-->