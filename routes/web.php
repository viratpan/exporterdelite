<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes();

Route::middleware('auth')->group(function () {
    
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/home', 'HomeController@index')->name('home');


    /*Agent*/
    Route::get('/agent', 'AgentController@index')->name('agent');
    Route::post('/agent/store', 'AgentController@store')->name('agent.store');
    Route::get('/agent/{id}', 'AgentController@index')->name('agent.edit');
    Route::get('/agent/update/{id}', 'AgentController@update')->name('agent.update');

    Route::get('/agentItem', 'AgentItemController@index')->name('agentItem');
    Route::post('/agentItem/store', 'AgentItemController@store')->name('agentItem.store');
    Route::get('/agentItem/{id}', 'AgentItemController@index')->name('agentItem.edit');
    Route::get('/agentItem/update/{id}', 'AgentItemController@update')->name('agentItem.update');
});
 Route::get('agent-data', 'AjaxController@getAgentData')->name('ajax.agent-data');
