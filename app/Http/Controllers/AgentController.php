<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Agent;

use App\Http\Controllers\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;

class AgentController extends Controller
{
    function __construct($foo = null)
    {
    	$this->middleware('auth');
    }

    public function index(Request $request,$id=null)
    {
    	
       $agent=Agent::all();
       $data =Agent::find($id);
       return view('agent.list', compact('agent','data'));
    }
    public function store(Request $request,$id=null)
    {
        $request->validate([
        	'name' => ['required', 'string'],
            'email' => ['required', 'string','unique:agents'],
            'mobile' => ['required', 'string'],
        ]);
        $data =new Agent;
            $data->name = $request->name;
            $data->email = $request->email;
            $data->mobile = $request->mobile;
            $data->gst = $request->gst;
            $data->pan = $request->pan;
            $data->address = $request->address;            
        $data->save();
        //die($password);
        return redirect()->route('agent')->with('success','Agent created successfully.');
    }



    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => ['required', 'string'],
            'email' => ['required', 'string','unique:agents'],
            'mobile' => ['required', 'string'],
        ]);

        $data = Agent::find($id);
            $data->name = $request->name;
            $data->email = $request->email;
            $data->mobile = $request->mobile;
            $data->gst = $request->gst;
            $data->pan = $request->pan;
            $data->address = $request->address;
            $data->save();

        return redirect()->route('agent')
                        ->with('success',$data->name.'  updated successfully');
    }


    public function destroy($id)
    {
         Agent::find($id)->delete();
        //$Lco = Lco::withTrashed()->get();

        return redirect()->route('agent')->with('success','Agent deleted successfully');
    }
}
