<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;


class AjaxController extends Controller
{
    public function getAgentData(Request $request)
    { 
        $data = DB::table("agents")
            ->where("id",$request->id)
            ->first();
        return response()->json($data);
    }
}
