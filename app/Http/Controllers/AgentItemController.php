<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Agent;
use App\Agentby;
use App\AgentItem;

use App\Http\Controllers\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;

class AgentItemController extends Controller
{
	function __construct($foo = null)
    {
    	$this->middleware('auth');
    }
    public function index(Request $request,$id=null)
    {
    
       $agent=Agent::all();
       $agentby=Agentby::all();
       $data =Agentby::find($id);
       $dataItem =DB::table('agent_items')->where('agentby',$id)->get();

       return view('agent.item-list', compact('agentby','data','dataItem','agent'));
    }
    public function store(Request $request,$id=null)
    {
    	//echo "<pre>",print_r($request->all()),'</pre>';
        $request->validate([
        	'name' => ['required', 'string'],
            'email' => ['required', 'string'],
            'mobile' => ['required', 'string'],
            'address' => ['required', 'string'],
            'item' => ['required', 'array'],
        ]);
        $data =new Agentby;
            $data->name = $request->name;
            $data->email = $request->email;
            $data->mobile = $request->mobile;
            $data->address = $request->address;
                                
        $data->save();
        if($data->id){
        	$items=$request->item;
        	foreach ($items as $key => $value) {
        		$data2 =new AgentItem;        		
		            $data2->name = $value['name'];
		            $data2->type = $value['type'];
		            $data2->rate = $value['rate'];
		            $data2->qty = $value['qty'];
		            $data2->amount = $value['amount'];
		            $data2->agentby=$data->id;      
		                             
		        $data2->save();		      
        	}
        }
        //die($password);
        return redirect()->route('agentItem')->with('success','Agent created successfully.');
    }
    public function update(Request $request, $id)
    {
         $request->validate([
        	'name' => ['required', 'string'],
            'email' => ['required', 'string'],
            'mobile' => ['required', 'string'],
            'address' => ['required', 'string'],
            'item' => ['required', 'array'],
        ]);

        $data = Agentby::find($id);
            $data->name = $request->name;
            $data->email = $request->email;
            $data->mobile = $request->mobile;
            $data->address = $request->address;
            $data->save();
        if($data->id){
        	$items=$request->item;
        	foreach ($items as $key => $value) {
        		if($value['id']){
        			$data2 =AgentItem::find($value['id']);      		
		            $data2->name = $value['name'];
		            $data2->type = $value['type'];
		            $data2->rate = $value['rate'];
		            $data2->qty = $value['qty'];
		            $data2->amount = $value['amount'];
		            $data2->agentby=$data->id;      
		                             
		        	$data2->save();
        		}else{
        			$data2 =new AgentItem;        		
		            $data2->name = $value['name'];
		            $data2->type = $value['type'];
		            $data2->rate = $value['rate'];
		            $data2->qty = $value['qty'];
		            $data2->amount = $value['amount'];
		            $data2->agentby=$data->id;      
		                             
		        	$data2->save();	
        		}
        			      
        	}
        }
        return redirect()->route('agentItem')->with('success',$data->name.'  updated successfully');
    }


    public function destroy($id)
    {
         Agent::find($id)->delete();
        //$Lco = Lco::withTrashed()->get();

        return redirect()->route('agent')->with('success','Agent deleted successfully');
    }
}
